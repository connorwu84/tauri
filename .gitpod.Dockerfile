# FROM ubuntu:20.04

# ENV DEBIAN_FRONTEND=noninteractive
# ENV TZ=America/New_York
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# RUN apt-get update

# RUN apt install -y libwebkit2gtk-4.0-dev \
#     build-essential \
#     curl \
#     wget \
#     libssl-dev \
#     libgtk-3-dev \
#     libappindicator3-dev \
#     librsvg2-dev

# https://community.gitpod.io/t/workspace-doesnt-start-with-custom-docker-image/3806/5
FROM alpine:3.15.0

RUN apk add --no-cache musl libc6-compat curl

RUN curl -o /usr/bin/lama -sSL https://github.com/csweichel/lama/releases/download/v0.3.0/lama_0.3.0_Linux_x86_64 \
    && chmod +x /usr/bin/lama

# Install nodejs & rust & cargo (rust)
RUN apk add curl wget bash util-linux file rust nodejs-current && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

# Install Tauri dependencies
# https://tauri.studio/docs/getting-started/setting-up-linux#1-system-dependencies
RUN apk add webkit2gtk-dev \
    alpine-sdk \ 
    wxgtk3-dev \
    libappindicator \
    librsvg-dev

